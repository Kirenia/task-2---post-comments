

async function getPost() {

   
    try {

         const response = await Poster.getPost();
         insertPosts(response);

    } catch (e) {
        console.log(e);
    }

}

function insertPosts(posts) {
    const elPosts = document.getElementById('post');
    elPosts.innerHTML='';
    posts.forEach(post => {
        elPosts.appendChild(Poster.createPostTemplate(post));});
}

getPost()
async function filtrerPost(){
 const search= document.getElementById('Search').value;

 try {
  const allPost= await Poster.getPost();
  const x = allPost.filter(post=> post.title.includes(search) || post.body.includes(search))
  insertPosts(x)

} catch (e) {
    console.log(e);
}
  
}