
async function getComments() {

    const elComments = document.getElementById('Comments');
 
    try {
        const response = await Poster.getComments(); 

        //Get url post value
        const urlParams = new URLSearchParams(window.location.search);
        const id =urlParams.get('post');
       //filter the comments by id
        const CommentsFilter = response.filter(comment=>comment.postId==id);
        CommentsFilter.forEach(comment => {
        elComments.appendChild(Poster.createCommentTemplate(comment));
        
        });

      

    } catch (e) {
        console.log(e);
    }

}

getComments();
