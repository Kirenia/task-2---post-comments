//IIFE.

const Poster = (function () {
    //Private

    //Public
    return {
        getPost() {
            return fetch('https://jsonplaceholder.typicode.com/posts').then(resp => resp.json());
        },
        createPostTemplate(post) {
            const postDiv = document.createElement('div');
            const postTitle = document.createElement('h1');
            const postBody = document.createElement('p');
            const postLink = document.createElement('a');
             

            postTitle.innerText = post.title;
            postBody.innerText = post.body;
            postLink.innerText='View comments';
            postLink.href = `/comments.html?post=${post.id}`;

            postBody.id= "IdBody";
            postDiv.className= "card border-primary mb-3";

            postDiv.appendChild(postTitle);
            postDiv.appendChild(postBody);
            postDiv.appendChild(postLink);
            return postDiv;

        },
        
    getComments(){
        return fetch('https://jsonplaceholder.typicode.com/comments').then(response =>response.json());
    },
     createCommentTemplate(comment){


        const commentList= document.createElement('div');
        const commentName= document.createElement('h2');
        const commentBody=document.createElement('p');
        const commentEmail=document.createElement('a');
        
         commentName.innerText= comment.name;
         commentBody.innerText=comment.body;
         commentEmail.innerText= "Autor Email :" + comment.email;
         
        commentList.className="card border-primary mb-3";
        commentEmail.id="email";
             
         commentList.appendChild(commentName);
         commentList.appendChild(commentBody);
         commentList.appendChild(commentEmail);
         return commentList;

     }
  
      
    
    }
  
    

})();
